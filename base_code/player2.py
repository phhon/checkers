# ======================== Class Player =======================================
import copy

import math


class Player:
    def __init__(self, str_name):
        self.str = str_name

    def __str__(self):
        return self.str

    # Student MUST implement this function
    # The return value should be a move that is denoted by a list of tuples

    # Create new state with move
    def generateNewState(move, state):
        new_state = copy.deepcopy(state)
        # Move one step
        # example: [(2,2),(3,3)] or [(2,2),(3,1)]
        if len(move) == 2 and abs(move[1][0] - move[0][0]) == 1:
            new_state[move[0][0]][move[0][1]] = '.'
            if state[move[0][0]][move[0][1]] == 'b' and move[1][0] == 7:
                new_state[move[1][0]][move[1][1]] = 'B'
            elif state[move[0][0]][move[0][1]] == 'r' and move[1][0] == 0:
                new_state[move[1][0]][move[1][1]] = 'R'
            else:
                new_state[move[1][0]][move[1][1]] = state[move[0][0]][move[0][1]]
        # Jump
        # example: [(1,1),(3,3),(5,5)] or [(1,1),(3,3),(5,1)]
        else:
            step = 0
            new_state[move[0][0]][move[0][1]] = '.'
            while step < len(move) - 1:
                new_state[int(math.floor((move[step][0] + move[step + 1][0]) / 2))][
                    int(math.floor((move[step][1] + move[step + 1][1]) / 2))] = '.'
                step = step + 1
            if state[move[0][0]][move[0][1]] == 'b' and move[step][0] == 7:
                new_state[move[step][0]][move[step][1]] = 'B'
            elif state[move[0][0]][move[0][1]] == 'r' and move[step][0] == 0:
                new_state[move[step][0]][move[step][1]] = 'R'
            else:
                new_state[move[step][0]][move[step][1]] = state[move[0][0]][move[0][1]]
        return new_state

    # Change Player
    def Opponent(namePlayer):
        if namePlayer == 'r':
            return 'b'
        else:
            return 'r'

    # Check valid move
    def isValidMove(state, index1, index2):
        if index2 < 0:
            return False
        elif index2 > 7:
            return False
        elif index1 < 0:
            return False
        elif index1 > 7:
            return False
        elif state[index1][index2] == '.':
            return True
        else:
            return False

    # Check valid jump
    def isValidJump(state, i, j, index1, index2, x1, x2):
        if index2 < 0:
            return False
        elif index2 > 7:
            return False
        elif index1 < 0:
            return False
        elif index1 > 7:
            return False
        elif state[int(math.floor((i + index1) / 2))][int(math.floor((j + index2) / 2))] in [x1, x2]:
            if state[index1][index2] == '.':
                return True
        else:
            return False

    # Jump generation
    def JumpGeneration(state, str):
        moveJump = []
        if str == 'r':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    dummy = [(i, j)]
                    if state[i][j] == 'r':
                        k = i
                        h = j
                        while (True):
                            if Player.isValidJump(state, k, h, k - 2, h - 2, 'b', 'B'):
                                dummy.append((k - 2, h - 2))
                                k = k - 2
                                h = h - 2
                            elif Player.isValidJump(state, k, h, k - 2, h + 2, 'b', 'B'):
                                dummy.append((k - 2, h + 2))
                                k = k - 2
                                h = h + 2
                            else:
                                break
                        if len(dummy) > 1:
                            moveJump.append(dummy)
                    elif state[i][j] == 'R':
                        k = i
                        h = j
                        newState = copy.deepcopy(state)
                        while (True):
                            # avoid jump back after jump forward
                            if len(dummy) > 1:
                                newState = Player.generateNewState(dummy, newState)
                            if Player.isValidJump(newState, k, h, k - 2, h - 2, 'b', 'B'):
                                dummy.append((k - 2, h - 2))
                                k = k - 2
                                h = h - 2
                            elif Player.isValidJump(newState, k, h, k - 2, h + 2, 'b', 'B'):
                                dummy.append((k - 2, h + 2))
                                k = k - 2
                                h = h + 2
                            elif (Player.isValidJump(newState, k, h, k + 2, h + 2, 'b', 'B')):
                                dummy.append((k + 2, h + 2))
                                k = k + 2
                                h = h + 2
                            elif (Player.isValidJump(newState, k, h, k + 2, h - 2, 'b', 'B')):
                                dummy.append((k + 2, h - 2))
                                k = k + 2
                                h = h - 2
                            else:
                                break
                        if (len(dummy) > 1):
                            moveJump.append(dummy)
            return moveJump
        else:
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    dummy = [(i, j)]
                    if state[i][j] == 'b':
                        k = i
                        h = j
                        while (True):
                            if (Player.isValidJump(state, k, h, k + 2, h + 2, 'r', 'R')):
                                dummy.append((k + 2, h + 2))
                                k = k + 2
                                h = h + 2
                            elif (Player.isValidJump(state, k, h, k + 2, h - 2, 'r', 'R')):
                                dummy.append((k + 2, h - 2))
                                k = k + 2
                                h = h - 2
                            else:
                                break
                        if (len(dummy) > 1):
                            moveJump.append(dummy)
                    elif state[i][j] == 'B':
                        k = i
                        h = j
                        newState = copy.deepcopy(state)
                        while (True):
                            # avoid jump back after jump forward
                            if len(dummy) > 1:
                                newState = Player.generateNewState(dummy, newState)
                            if Player.isValidJump(newState, k, h, k - 2, h - 2, 'r', 'R'):
                                dummy.append((k - 2, h - 2))
                                k = k - 2
                                h = h - 2
                            elif (Player.isValidJump(newState, k, h, k - 2, h + 2, 'r', 'R')):
                                dummy.append((k - 2, h + 2))
                                k = k - 2
                                h = h + 2
                            elif (Player.isValidJump(newState, k, h, k + 2, h + 2, 'r', 'R')):
                                dummy.append((k + 2, h + 2))
                                k = k + 2
                                h = h + 2
                            elif (Player.isValidJump(newState, k, h, k + 2, h - 2, 'r', 'R')):
                                dummy.append((k + 2, h - 2))
                                k = k + 2
                                h = h - 2
                            else:
                                break
                        if (len(dummy) > 1):
                            moveJump.append(dummy)
            return moveJump

    # move generation
    def moveGeneration(state, str):
        move = []
        if str == 'r':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'r':
                        # Right forward
                        if Player.isValidMove(state, i - 1, j - 1):
                            move.append([(i, j), (i - 1, j - 1)])
                        # Left forward
                        if Player.isValidMove(state, i - 1, j + 1):
                            move.append([(i, j), (i - 1, j + 1)])
                    elif state[i][j] == 'R':
                        # right forward
                        if Player.isValidMove(state, i - 1, j - 1):
                            move.append([(i, j), (i - 1, j - 1)])
                        # left forward
                        if Player.isValidMove(state, i - 1, j + 1):
                            move.append([(i, j), (i - 1, j + 1)])
                        # right backward
                        if Player.isValidMove(state, i + 1, j + 1):
                            move.append([(i, j), (i + 1, j + 1)])
                        # left backward
                        if Player.isValidMove(state, i + 1, j - 1):
                            move.append([(i, j), (i + 1, j - 1)])
            return move
        else:
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'b':
                        # right forward
                        if Player.isValidMove(state, i + 1, j + 1):
                            move.append([(i, j), (i + 1, j + 1)])
                        # left forward
                        if Player.isValidMove(state, i + 1, j - 1):
                            move.append([(i, j), (i + 1, j - 1)])
                    elif state[i][j] == 'B':
                        # right forward
                        if (Player.isValidMove(state, i - 1, j - 1)):
                            move.append([(i, j), (i - 1, j - 1)])
                        # left forward
                        if (Player.isValidMove(state, i - 1, j + 1)):
                            move.append([(i, j), (i - 1, j + 1)])
                        ############# backward ################
                        # right backward
                        if (Player.isValidMove(state, i + 1, j + 1)):
                            move.append([(i, j), (i + 1, j + 1)])
                        # left backward
                        if Player.isValidMove(state, i + 1, j - 1):
                            move.append([(i, j), (i + 1, j - 1)])
            return move

    # Move or jump generation
    def moveOrJumpGeneration(state, str):
        jump = Player.JumpGeneration(state, str)
        if not jump:
            # print('place')
            move = Player.moveGeneration(state, str)
            # print(placeMoves)
            return move
        # print('jump')
        # print(jumpMoves)
        return jump

    # count my King
    def countKing(state, namePlayer):
        countKingPlayer = 0
        countKingOpponent = 0
        countPawPlayer = 0
        countPawOpponent = 0
        if namePlayer == 'r':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'R':
                        countKingPlayer = countKingPlayer + 1
                    elif state[i][j] == 'B':
                        countKingOpponent = countKingOpponent + 1
                    elif state[i][j] == 'r':
                        countPawPlayer = countPawPlayer + 1
                    elif state[i][j] == 'b':
                        countPawOpponent = countPawOpponent + 1
            return countKingPlayer, countKingOpponent, countPawPlayer, countPawOpponent
        elif namePlayer == 'b':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'B':
                        countKingPlayer = countKingPlayer + 1
                    elif state[i][j] == 'R':
                        countKingOpponent = countKingOpponent + 1
                    elif state[i][j] == 'b':
                        countPawPlayer = countPawPlayer + 1
                    elif state[i][j] == 'r':
                        countPawOpponent = countPawOpponent + 1
            return countKingPlayer, countKingOpponent, countPawPlayer, countPawOpponent

    # Static valuation end game function
    def allDistances(state, namePlayer):
        distance = 0
        check = False
        if namePlayer == 'r':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'R':
                        check = False
                        for h in [7, 6, 5, 4, 3, 2, 1, 0]:
                            for k in range(8):
                                if state[h][k] == 'B':
                                    distance = distance + (h - i) + abs(k - j)
                                    check = True
                                    break
                            if check == True:
                                break

            return distance
        if namePlayer == 'b':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'B':
                        check = False
                        for h in [7, 6, 5, 4, 3, 2, 1, 0]:
                            for k in range(8):
                                if state[h][k] == 'R':
                                    distance = distance + (h - i) + abs(k - j)
                                    check = True
                                    break
                            if (check == True):
                                break
            return distance

    # evaluation end game
    def staticValueEndGame(state, namePlayer):
        score = 0
        if namePlayer == 'r':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'r':
                        score = score + 5
                    elif state[i][j] == 'R':
                        score = score + 5 + 8
                    elif state[i][j] == 'b':
                        score = score - 5 - 5 - 50
                    elif state[i][j] == 'B':
                        score = score - 5 - 8 - 100
            return score
        elif namePlayer == 'b':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'b':
                        score = score + 5
                    elif state[i][j] == 'B':
                        score = score + 5 + 8
                    elif state[i][j] == 'r':
                        score = score - 5 - 5
                    elif state[i][j] == 'R':
                        score = score - 5 -8 -10
            return score

    # count all pieces
    def countAllPie(state):
        count = 0
        for i in [7, 6, 5, 4, 3, 2, 1, 0]:
            for j in range(8):
                if state[i][j] == 'b':
                    count = count + 1
                elif state[i][j] == 'B':
                    count = count + 1
                elif state[i][j] == 'r':
                    count = count + 1
                elif state[i][j] == 'R':
                    count = count + 1
        return count
    # Static valuation function
    def staticValue(state, namePlayer, depth):
        score = 0
        #countKingPlayer, countKingOpponent, countPawPlayer, countPawOpponent = Player.countKing(state, namePlayer)
        #if countKingPlayer > 3 and countKingPlayer > countKingOpponent:
            #score = Player.allDistances(state, namePlayer)
            #return score
        count = Player.countAllPie(state)
        if count < 7:
            return Player.staticValueEndGame(state, namePlayer)

        if namePlayer == 'r':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'r':
                        score = score + 5 + (7 - i)
                    elif state[i][j] == 'R':
                        score = score + 5 + 8 + 2
                    elif state[i][j] == 'b':
                        score = score - 5 - i
                    elif state[i][j] == 'B':
                        score = score - 5 - 8 - 2
            return score
        elif namePlayer == 'b':
            for i in [7, 6, 5, 4, 3, 2, 1, 0]:
                for j in range(8):
                    if state[i][j] == 'b':
                        score = score + 5 + i
                    elif state[i][j] == 'B':
                        score = score + 5 + 8 + 2
                    elif state[i][j] == 'r':
                        score = score - 5 - (7 - i)
                    elif state[i][j] == 'R':
                        score = score - 5 - 8 - 2
            return score




    # Minimax
    def miniMax(selft, state, depth, namePlayer):
        bestMove = []
        bestScore = -1000
        # depth ennough
        if depth == 0:
            return Player.staticValue(state, selft.__str__(), depth), []
        moves = Player.moveOrJumpGeneration(state, namePlayer)
        # empty successors
        if not moves:
            return Player.staticValue(state, selft.__str__(), depth), []
        else:
            for move in moves:
                nextState = Player.generateNewState(move, state)
                resultSucc = Player.miniMax(selft, nextState, depth - 1, Player.Opponent(namePlayer))
                newValue = -resultSucc[0]
                if newValue > bestScore:
                    bestScore = newValue
                    bestMove = move
            return bestScore, bestMove

    def nextMove(selft, state):
        newState = copy.deepcopy(state)
        # print(state)
        result = Player.miniMax(selft, newState, 4, selft.__str__())
        return result[1]
