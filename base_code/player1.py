
# ======================== Class Player =======================================
from functools import reduce
from random import randint
import math
import time


class Player:

  def __init__(self, str_name):
    self.str = str_name
    self.score = 0
    self.state = []
    self.start = 0

  def __str__(self):
    return self.str

  @staticmethod
  def distance(cell1, cell2):
    return int(((cell1[1] - cell2[1]) ** 2 + (cell1[0] - cell2[0]) ** 2) ** 0.5)

  @staticmethod
  def isTriangle(state, str):
    if str == 'r':
      return (state[7][1]).lower() == (state[7][3]).lower() == (state[6][2]).lower() == 'r'
    if str == 'b':
      return (state[0][2]).lower() == (state[0][4]).lower() == (state[1][3]).lower() == 'b'

  @staticmethod
  def isOreo(state, str):
    if str == 'r':
      return (state[7][5]).lower() == (state[7][3]).lower() == (state[6][4]).lower() == 'r'
    if str == 'b':
      return (state[0][6]).lower() == (state[0][4]).lower() == (state[1][5]).lower() == 'b'

  @staticmethod
  def isDog(state, str):
    if str == 'r':
      return (state[7][5]).lower() == (state[7][1]).lower() == 'r'
    if str == 'b':
      return (state[0][6]).lower() == (state[0][2]).lower() == 'b'

  @staticmethod
  def isBridge(state, str):
    if str == 'r':
      return (state[7][1]).lower() == (state[6][0]).lower() == 'r'
    if str == 'b':
      return (state[0][6]).lower() == (state[1][7]).lower() == 'b'

  @staticmethod
  def isManCorner(state, str):
    if str == 'r':
      return state[7][7] == 'r'
    if str == 'b':
      return state[0][0] == 'b'

  @staticmethod
  def isKingCorner(state, str):
    if str == 'r':
      return state[0][0] == 'R'
    if str == 'b':
      return state[7][7] == 'B'

  @staticmethod
  def isInEdge(cell):
    return cell[1] == 0 or cell[1] == 7

  @staticmethod
  def movable(state, cell):
    backward = [(-1, -1), (-1, 1)]
    forward = [(1, -1), (1, 1)]
    name = Player.getCellName(state, cell)
    directions = {
      'R': backward + forward,
      'B': backward + forward,
      'r': backward,
      'b': forward,
      '.': []
    }
    result = False
    for direction in directions[name]:
      row = cell[0] + direction[0]
      col = cell[1] + direction[1]
      result = result or (Player.inBoard(row, col) and Player.notCaptured(state, row, col))

  @staticmethod
  def isAttacker(cell, str):
    if str == 'r':
      return cell[0] <= 2
    if str == 'b':
      return cell[0] >= 5

  @staticmethod
  def isDefender(cell, str):
    if str == 'r':
      return cell[0] >= 6
    if str == 'b':
      return cell[0] <= 1

  @staticmethod
  def isCentral(cell, str):
    if str == 'r':
      return 3 <= cell[0] <= 5
    if str == 'b':
      return 2 <= cell[0] <= 4

  @staticmethod
  def isInMainDiagonal(cell):
    return cell[0] == cell[1]

  @staticmethod
  def isInDoubleDiagonal(cell):
    return abs(cell[0] - cell[1]) == 2

  @staticmethod
  def adjacentCells(cell):
    directions = [(-1, -1), (-1, 1), (1, -1), (1, 1)]
    return [(cell[0] + d[0], cell[1] + d[1]) for d in directions if Player.inBoard(cell[0] + d[0], cell[1] + d[1])]
  @staticmethod
  def isHole(state, cell, str):
    adjacents = Player.adjacentCells(cell)
    return len([adj for adj in adjacents if Player.getCellName(state, cell).lower() == str]) >= 3

  @staticmethod
  def isLoner(state, cell):
    adjacents = Player.adjacentCells(cell)
    return len([adj for adj in adjacents if Player.getCellName(state, adj) == '.'])

  @staticmethod
  def staticEvaluation(state, str):
    myMan = str
    myKing = str.upper()
    opponentMan = Player.opponent(str)
    opponentKing = opponentMan.upper()
    simpleFeature = {
      '.': 0,
      myKing: 0,
      myMan: 0,
      opponentMan: 0,
      opponentKing: 0,
      myMan + '_edge': 0,
      opponentMan + '_edge': 0,
      myMan + '_movable': 0,
      opponentMan + '_movable': 0,

    }

    myManPromotionLine = []

    opponentManPromotionLine = []

    myManEmptyCellPromotionLine = []

    opponentManEmptyCellPromotionLine = []

    layoutFeature = {
      myMan + '_defender': 0, # 2 lowest rows
      opponentMan + '_defender': 0,
      myMan + '_attacker': 0, # 3 topmost rows
      opponentMan + '_attacker': 0,
      myMan + '_central': 0,
      opponentMan + '_central': 0,
      myKing + '_central': 0,
      opponentKing + '_central': 0,
      myMan + '_mainDiagonal': 0, # duong cheo chinh
      opponentMan + '_mainDiagonal': 0,
      myKing + '_mainDiagonal': 0,
      opponentKing + '_mainDiagonal': 0,
      myMan + '_doubleDiagonal': 0,
      opponentMan + '_doubleDiagonal': 0, # 2 duong cheo ke duong cheo chinh
      myKing + '_doubleDiagonal': 0,
      opponentKing + '_doubleDiagonal': 0,
      myMan + '_loner': 0, # not adjacent to any other pieces
      opponentMan + '_loner': 0,
      myMan + '_holes': 0, # empty cell adjacent to at least 3 of a color
      myKing + '_holes': 0
    }

    patternFeature = {
      '27_31_32': 0, # white man - triangle
      '26_30_31': 0, # white man - oreo
      '30_32': 0, # white man - bridge
      '32_28': 0, # dog - white man 32 black man 28
      '29': 0, # white - man corner on 29
      '4': 0 # white - king corner 4
    }
    for row in state:
      for cell in row:
        print(cell)


  @staticmethod
  def calculateScore(state, str):
    myMan = str
    myKing = str.upper()
    opponentMan = Player.opponent(str)
    opponentKing = opponentMan.upper()
    score = 0
    deltaScore = {
      myMan: 10,
      myKing: 20,
      opponentMan: -10,
      opponentKing: -20,
      '.': 0
    }

    allMyCells = Player.allMyCells(state, str)
    allOpponentCells = Player.allMyCells(state, opponentMan)

    for cell in allMyCells + allOpponentCells:
      score += deltaScore[Player.getCellName(state, cell)]

    kingWall = {
      'r': 7,
      'R': 7,
      'b': 0,
      'B': 0
    }

    for me in allMyCells:
      score -= int(math.fabs(me[0] - kingWall[str] / 4))

    for opponent in allOpponentCells:
      score += int(math.fabs(opponent[0] - kingWall[opponentMan] / 4))

    # maxDistance = Player.distance((0,0), (7,7))

    # # distance
    # for me in allMyCells:
    #   for opponent in allOpponentCells:
    #     if len(allMyCells) > (len(allOpponentCells) + 1): # try to kill
    #       score -= int(Player.distance(me, opponent) * 2 / maxDistance)
    #     else: # try to defend
    #       score += int(Player.distance(me, opponent) * 2 / maxDistance)

    return score

  @staticmethod
  def allMyCells(state, str):
    result = []
    row = 0
    while (row < len(state)):
      col = 0
      while (col < len(state[row])):
        if ((state[row][col]).lower() == str):
          result.append((row, col))
        col = col + 1
      row = row + 1
    # print(str + ' cells ' + result.__str__())
    return result

  @staticmethod
  def inBoard(row, col):
    return (0 <= row <= 7) and (0 <= col <= 7)

  @staticmethod
  def notCaptured(state, row, col):
    return state[row][col] == '.'

  @staticmethod
  def isOpponent(state, row, col, name):
    # print('check ' + (row, col).__str__() + ' opponent of ' + str )
    if str == '-' or str == '.':
      return False
    if (name.lower() == 'r'):
      return (state[row][col]).lower() == 'b'
    elif name.lower() == 'b':
      return (state[row][col]).lower() == 'r'

  @staticmethod
  def opponent(str):
    return 'r' if str=='b' else 'b'

  @staticmethod
  def getCellName(state, cell):
    return state[cell[0]][cell[1]]

  @staticmethod
  def tryPlace(state, cell):
    backward = [(-1, -1), (-1, 1)]
    forward = [(1, -1), (1, 1)]
    name = Player.getCellName(state, cell)
    directions = {
      'R': backward + forward,
      'B': backward + forward,
      'r': backward,
      'b': forward,
      '.': []
    }
    result = []
    for direction in directions[name]:
      row = cell[0] + direction[0]
      col = cell[1] + direction[1]
      if Player.inBoard(row, col) and Player.notCaptured(state, row, col):
        result.append((row, col))
    return result

  @staticmethod
  def tryJump(state, cell, name):
    result = []

    backward = [(-2, -2), (-2, 2)]
    forward = [(2, -2), (2, 2)]

    directions = {
      'R': backward + forward,
      'B': backward + forward,
      'r': backward,
      'b': forward,
      '.': []
    }

    for direction in directions[name]:
      row = cell[0] + direction[0]
      col = cell[1] + direction[1]
      if Player.inBoard(row, col) and Player.notCaptured(state, row, col):
        manInTheMiddle = (int((cell[0] + row) / 2), int((cell[1] + col)/2))
        manInTheMiddleName = state[manInTheMiddle[0]][manInTheMiddle[1]]
        if Player.isOpponent(state, manInTheMiddle[0], manInTheMiddle[1], name):
          state[manInTheMiddle[0]][manInTheMiddle[1]] = '-' # mark he is dead
          jumpAgain = Player.tryJump(state, (row, col), name)
          if jumpAgain:
            # result.extend(list(map(lambda move: [cell] + move, jumpAgain)))
            result.extend([[cell] + move for move in jumpAgain])
            # print((row, col).__str__() + 'JUMP AGAIN LEFT' + jumpAgain.__str__())
          else:
            result.append([cell, (row, col)])
          state[manInTheMiddle[0]][manInTheMiddle[1]] = manInTheMiddleName  # rollback
    return result

  @staticmethod
  def moveGen(state, str):
    allCells = Player.allMyCells(state, str)
    jumpMoves = [move for cell in allCells for move in Player.tryJump(state, cell, Player.getCellName(state,cell))]
    if not jumpMoves:
      # print('place')
      placeMoves = [[cell, nextCell] for cell in allCells for nextCell in Player.tryPlace(state, cell)]
      # print(placeMoves)
      return placeMoves
    # print('jump')
    # print(jumpMoves)
    return jumpMoves

  @staticmethod
  def stateCopy(state):
    new_board = [[]] * 8
    for i in range(8):
      new_board[i] = [] + state[i]
    return new_board

  @staticmethod
  def generateNewState(move, state):
    new_state = Player.stateCopy(state)
    # Move one step
    # example: [(2,2),(3,3)] or [(2,2),(3,1)]
    if len(move) == 2 and abs(move[1][0] - move[0][0]) == 1:
      new_state[move[0][0]][move[0][1]] = '.'
      if state[move[0][0]][move[0][1]] == 'b' and move[1][0] == 7:
        new_state[move[1][0]][move[1][1]] = 'B'
      elif state[move[0][0]][move[0][1]] == 'r' and move[1][0] == 0:
        new_state[move[1][0]][move[1][1]] = 'R'
      else:
        new_state[move[1][0]][move[1][1]] = state[move[0][0]][move[0][1]]
    # Jump
    # example: [(1,1),(3,3),(5,5)] or [(1,1),(3,3),(5,1)]
    else:
      step = 0
      new_state[move[0][0]][move[0][1]] = '.'
      while step < len(move) - 1:
        new_state[int(math.floor((move[step][0] + move[step + 1][0]) / 2))][
          int(math.floor((move[step][1] + move[step + 1][1]) / 2))] = '.'
        step = step + 1
      if state[move[0][0]][move[0][1]] == 'b' and move[step][0] == 7:
        new_state[move[step][0]][move[step][1]] = 'B'
      elif state[move[0][0]][move[0][1]] == 'r' and move[step][0] == 0:
        new_state[move[step][0]][move[step][1]] = 'R'
      else:
        new_state[move[step][0]][move[step][1]] = state[move[0][0]][move[0][1]]
    return new_state

  @staticmethod
  def depthEnough(depth):
    return depth >= 4

  @staticmethod
  def chooseStaticMove(allMoves, state, name):
    opponent = Player.opponent(name)
    bestMoves = []
    worstOpponentScore = math.inf
    for move in allMoves:
      nextState = Player.generateNewState(move, state)
      score = Player.calculateScore(nextState, opponent)
      # print('SCORE ' + score.__str__())
      if (score < worstOpponentScore):
        worstOpponentScore = score
        bestMoves = [move]
      elif score == worstOpponentScore:
        bestMoves.append(move)

    movesCount = len(bestMoves)
    #print('best score ' + worstOpponentScore.__str__() + ' move list ' + bestMoves.__str__())
    # print('ALL MOVES ' + allMoves.__str__())
    result = bestMoves[randint(0, movesCount - 1)] if movesCount > 0 else []
    # print(result.__str__())
    return result

  @staticmethod
  def chooseByOpponentMove(allMoves, state, name, depth, start):
    opponent = Player.opponent(name)
    bestMoves = []
    worstOpponentScore = math.inf
    for move in allMoves:
      nextState = Player.generateNewState(move, state)
      nextOpponentMove = Player.minimax(nextState, opponent, depth + 1, start)
      if (not nextOpponentMove) or (time.time() - start > 2.7):
        return Player.chooseStaticMove(allMoves, state, name)
      # print(nextOpponentMove)
      stateAfterOpponentMove = Player.generateNewState(nextOpponentMove, nextState)
      score = Player.calculateScore(stateAfterOpponentMove, opponent)
      # print('SCORE ' + score.__str__())
      if (score < worstOpponentScore):
        worstOpponentScore = score
        bestMoves = [move]
      elif score == worstOpponentScore:
        bestMoves.append(move)

    movesCount = len(bestMoves)
    result = bestMoves[randint(0, movesCount - 1)] if movesCount > 0 else []
    return result

  @staticmethod
  def minimax(state, name, depth, start):
    allMoves = Player.moveGen(state, name)
    if not allMoves:
      return []

    if not Player.depthEnough(depth):
      # print(result.__str__())
      return Player.chooseByOpponentMove(allMoves, state, name, depth, start)
    else:
      # print('enoguth')
      return Player.chooseStaticMove(allMoves, state, name)

  # Student MUST implement this function
  # The return value should be a move that is denoted by a list of tuples
  def nextMove(self, state):
    start = time.time()
    state = Player.stateCopy(state)
    #result = Player.anyPlayer(self.state, self.str, 1)
    result = Player.minimax(state, self.str, 0, start)
    # print('result of nextMove ' + result.__str__())
    return result